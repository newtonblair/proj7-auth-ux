import os
import flask
from flask import Flask, redirect, url_for, request, render_template, session, flash, abort, jsonify, request, Response
from pymongo import MongoClient
####
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import logging
import requests
###### imports for login
from functools import wraps
from flask_wtf import FlaskForm
from flask_login import LoginManager, current_user, login_required, login_user, logout_user, UserMixin, confirm_login, fresh_login_required
from wtforms import Form, BooleanField, StringField, validators, PasswordField
from passlib.apps import custom_app_context as pwd_context
#####imports for token
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired
import time
#######api
from flask_restful import Resource, Api, fields, marshal_with
import os
import csv


###
# Globals
###
app = Flask(__name__)
api = Api(app)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
idcount = 0
USERS = {}

########init login handler
login_manager = LoginManager();
login_manager.setup_app(app);
login_manager.login_view = 'login' ##added the / this may break the code


client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb


############user init
class User(UserMixin):
    def __init__(self, name, id, password, active=True):
        self.name = name
        self.id = id
        self.password = password
        self.active = active

    def is_active(self):
        return self.active

    def verify_password(self, password):
        return pwd_context.verify(password, self.password)

    def get_auth_token(self, expiration=600):
        s = Serializer(app.secret_key, expires_in=expiration)
        return s.dumps({'id':self.id})

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(app.secret_key)
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None    # valid token, but expired
        except BadSignature:
            return None    # invalid token
        return "Success"


#########################################################################
@app.route('/done', methods=["POST"])
def todo():
    _items = db.tododb.find()
    items = [item for item in _items]
    return flask.render_template('todo.html', items=items)

@app.route('/new', methods=['POST'])
def new():
    db.tododb.remove({})
    km_vals = []
    open_vals = []
    close_vals = []
    mult = request.form
    for val in mult.getlist('km'):
        if val != '':
            print("still looping")
            km_vals.append(val)
        else:
            break
    length = len(km_vals)
    if length == 0:
        return flask.render_template('empty_form.html')
    else:
        needed = {'open':open_vals, 'close':close_vals}
        last_keys = list(needed.keys())
        for key in last_keys:
            for val in mult.getlist(key):
                if val != '':
                    selectList = needed[key]
                    selectList.append(val)
                else:
                    break
        for i in range(length):
            item_doc = {
                'distance': request.form['distance'],
                'km': km_vals[i],
                'open': open_vals[i],
                'close': close_vals[i]
            }
            app.logger.debug("Here is an extracted val: {} ".format(item_doc))
            db.tododb.insert_one(item_doc)
    return redirect(url_for("index"))


######Stuff from flask_brevets

###
# Pages
###


@app.route("/")
@app.route("/index")
@login_required
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')                           #Renders the main page for the website


# @app.errorhandler(404)
# def page_not_found(error):
#     app.logger.debug("Page not found")
#     flask.session['linkback'] = flask.url_for("index")                  #Attempts to re-render the main page
#     return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    begin_time = request.args.get('begin_time')                      #grabs the begin_time data from html page
    begin_date = request.args.get('begin_date')                      #grabs the begin_date data from html page
    distance = float(request.args.get('distance'))                   #grabs the distance data from html page
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    if km >= (1.2*distance):                                         #if checkpoint is 20% larger than distance => error
        result = {"error":-1}
        return flask.jsonify(result=result)                          #return error to raise error code
    start_date = begin_date+" "+begin_time                           #formats date and time into a string to be passed into arrow 
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    open_time = acp_times.open_time(km, distance, start_date)        #solve for open time of checkpoint
    close_time = acp_times.close_time(km, distance, start_date)      #solve for close time of checkpoint
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


################ Forms for login and Registration
class registrationForm(FlaskForm):
    username = StringField('Create Username', [validators.InputRequired("A username is required")])
    password = PasswordField('Create Password', [validators.InputRequired("A password is required"), validators.EqualTo('confirmpass', message='Passwords must match')])
    confirmpass = PasswordField('Confirm Password')

class loginForm(FlaskForm):
    username = StringField('Username', [validators.InputRequired()])
    password = PasswordField('Password', [validators.InputRequired()])
    rememberme = BooleanField('Remember Me')


################# functions and routing for logging in and regestering

@app.route("/api/register", methods=["GET", "POST"])
def register():
    global idcount
    global USERS
    username = request.json.get('username')
    app.logger.debug(username)
    if username:
        if request.json.get('password'):
            hashpass = pwd_context.encrypt(request.json.get('password'))
            if db.users.find({'username':username}).count() == 0:
                uid = str(idcount)
                acct = User(username, uid, hashpass)
                USERS[idcount] = acct
                idcount+=1
                person = {'username': username, 'pass': hashpass, 'id': uid}
                db.users.insert_one(person)
                return (jsonify({'username': acct.name}), 201, {'Location': url_for('get_user', id=int(acct.id), _external=True)})
    abort(400)

@app.route("/frontregister", methods=["GET", "POST"])
def frontregister():
    global idcount
    global USERS
    form = registrationForm()
    if form.validate_on_submit():
        username = form.username.data
        if db.users.find({'username':username}).count() == 0:
            password = form.password.data
            hashpass = pwd_context.encrypt(password)
            del(password)
            uid = str(idcount)
            acct = User(username, uid, hashpass)
            USERS[idcount] = acct
            idcount+=1
            person = {'username': username, 'pass': hashpass, 'id': uid}
            db.users.insert_one(person)
            return (jsonify({'username': acct.name}), 201, {'Location': url_for('get_user', id=int(acct.id), _external=True)})
        else:
            flash("Username is already taken")
            return render_template('register.html', form=form), 400 

    else:
        return render_template('register.html', form=form), 400

@app.route("/login", methods=["GET", "POST"])
def login():
    form = loginForm()
    if form.validate_on_submit():
        username = form.username.data
        if db.users.find({'username':username}).count() != 0:
            acct = db.users.find({'username':username})
            for val in acct:
                if pwd_context.verify(form.password.data, val['pass']):
                    if login_user(USERS[int(val['id'])], remember=form.rememberme.data):
                        acct = USERS.get(int(val['id']))
                        token = acct.get_auth_token()
                        session['api_session_token'] = token
                        return redirect(url_for('index'))
                    else:
                        return render_template('login.html', form=form)

                else:
                    abort(401)
        else:
            return render_template("login.html", form=form)
    else:
        return render_template('login.html', form=form)

@app.route("/api/user/<int:id>")
def get_user(id):
    user = USERS.get(id)
    if user:
        return jsonify({'username': user.name})
    abort(400)


@app.route("/api/token", methods=["GET"])
def gen_auth_token():
    username = request.authorization['username']
    password = request.authorization['password']
    if db.users.find({'username': username}).count() != 0:
        acct = db.users.find({'username': username})
        for val in acct:
            if pwd_context.verify(password, val['pass']):
                id = val['id']
                user = USERS.get(int(id))
                login_user(user)
                if user.is_authenticated:
                    token = (user.get_auth_token()).decode("utf-8", errors="ignore")
                    expire = 600
                    return jsonify({'token':token, 'duration':expire})
                else:
                    abort(401)
            else:
                abort(401)
    else:
        abort(401)

#verify the token
def verify_auth_token(token):
    s = Serializer(app.secret_key)
    try:
        data = s.loads(token)
    except SignatureExpired:
        return None    # valid token, but expired
    except BadSignature:
        return None    # invalid token
    return "Success"

def requires_token(func):
    @wraps(func)
    def token_check(*args, **kwargs):
        if request.authorization is None:
            if 'api_session_token' not in session:
                abort(401)
            if current_user.verify_auth_token(session['api_session_token']) is None:
                abort(401)
        elif verify_auth_token((request.authorization['username']).encode("utf-8", errors="ignore")) is None:
            if 'api_session_token' not in session:
                abort(401)
            if current_user.verify_auth_token(session['api_session_token']) is None:
                abort(401)
        return func(*args, **kwargs)
    return token_check

@login_manager.user_loader
def load_user(id):
    return USERS.get(int(id))

@app.route("/logout", methods=["GET", "POST"])
@login_required
def logout():
    logout_user()
    flash("Logged out.")
    return redirect(url_for("login"))
######################################################Api
class listAll(Resource):
    method_decorators = [requires_token]
    allfields = {                                                     #dictionary of the keys I want in the returned data
        'open': fields.String,
        'close': fields.String
    }
    @marshal_with(allfields)                                          #filters return to only have keys from the allfields dict
    def get(self):
        _items = db.tododb.find()
        items = [item for item in _items]
        return items

class listOpenOnly(Resource):
    method_decorators = [requires_token]
    openfields = {
        'open': fields.String
    }
    @marshal_with(openfields)
    def get(self):
        _items = db.tododb.find()
        items = [item for item in _items]
        if request.args.get('top') is None:                           #if there is no top argument in the url just return all
            return items
        else:
            num = request.args.get('top', 0, type=int)
            resized_items = []
            if num > len(items):                                     #if the argument in the url is bigger than len of data just return avaiable data
                return items
            else:
                for i in range(num):                                 #else only get the designated num number of inputs to return
                    resized_items.append(items[i])
                return resized_items


class listCloseOnly(Resource):
    method_decorators = [requires_token]
    closefields = {
        'close': fields.String
    }
    @marshal_with(closefields)
    def get(self):
        _items = db.tododb.find()
        items = [item for item in _items]
        if request.args.get('top') is None:
            return items
        else:
            num = request.args.get('top', 0, type=int)
            resized_items = []
            if num > len(items):
                return items
            else:
                for i in range(num):
                    resized_items.append(items[i])
                return resized_items


api.add_resource(listAll, '/listAll', '/listAll/json')                              #add uri to the resources
api.add_resource(listOpenOnly, '/listOpenOnly', '/listOpenOnly/json')
api.add_resource(listCloseOnly, '/listCloseOnly', '/listCloseOnly/json')

#note: csv rewrite was based off following resource http://blog.appliedinformaticsinc.com/how-to-parse-and-convert-json-to-csv-using-python/
class listAllcsv(Resource):
    method_decorators = [requires_token]
    def get(self):
        r = listAll.get(listAll)                                           #get array from the listAll resource get function
        outfile = open("listAll.csv", "w")                                 #open new file in write mode
        writer = csv.writer(outfile)                                      #set writer to write into file
        count = 0
        for item in r:
            if count == 0:
                header = item.keys()                                      #gets keys from the dataset
                writer.writerow(header)                                   #write the headers as a row
                count += 1
            writer.writerow(item.values())                                #write the values as a row under correlating header
        outfile.close()
        csvfile = open("listAll.csv", "r")

        return Response(csvfile, mimetype="text/csv")                    #return the opened file in read mode

class listOpenOnlycsv(Resource):
    method_decorators = [requires_token]
    def get(self):
        r = listOpenOnly.get(listOpenOnly)                      #get array from the listOpenOnly resource get function
        outfile = open("listOpenOnly.csv", "w")
        writer = csv.writer(outfile)
        count = 0
        for item in r:
            if count == 0:
                header = item.keys()                             #gets keys from the dataset
                writer.writerow(header)                          #write the headers as a row
                count += 1
            writer.writerow(item.values())                       #write the values as a row under correlating header
        outfile.close()
        csvfile = open("listOpenOnly.csv", "r")

        return Response(csvfile, mimetype="text/csv")            #returns csv file

class listCloseOnlycsv(Resource):                                #resource follows same format as the above two other resources
    method_decorators = [requires_token]
    def get(self):
        r = listCloseOnly.get(listCloseOnly)
        outfile = open("listCloseOnly.csv", "w")
        writer = csv.writer(outfile)
        count = 0
        for item in r:
            if count == 0:
                header = item.keys()
                writer.writerow(header)
                count += 1
            writer.writerow(item.values())
        outfile.close()
        csvfile = open("listCloseOnly.csv", "r")

        return Response(csvfile, mimetype="text/csv")

api.add_resource(listAllcsv,'/listAll/csv')                             #add uri for the resources 
api.add_resource(listOpenOnlycsv, '/listOpenOnly/csv')
api.add_resource(listCloseOnlycsv, '/listCloseOnly/csv')

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
