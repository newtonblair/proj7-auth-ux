## Student Author and Contact Info
Newton Blair 
email: nblair@uoregon.edu


## Software Description
This software is an extenstion of my Project 6. For it I utilize 2 containers one for the database and one for the the website. Upon Starting this software go to 127.0.0.1:5000 this will redirect you to a login page, unless you have already logged in and have a cookie/token in which case it will render the acp brevets page, you can click on the register link to register an account, and then go back to the login page, login, and then you should be able to access the brevets calculator page. From here you will input your data and then you can access the resouces created in project 6 as long as you have an active session token. See specs for more.
Once you log in your acct should have an active session token, that allows you to get the resources that are token authentication protected. This token lasts 10 minutes and once it expires you have to re-login to get a fresh new token (if using the UI). If using curl commands you can get a new token at the "localhost/api/token" url.

to logout simply go to 127.0.0.1:5000/logout this logs you out and redirects you to the log in page, or press the logout button at the bottom of the acp brevets calculator page.

## How to User curl commands for this software
To register a user via curl use the following command:
$ curl -i -X POST -H "Content-Type: application/json" -d '{"username":"(desired_username)","password":"(desired_password)"}' http://127.0.0.1:5000/api/register


To get a token for the registered user use the following command
$ curl -u (your_username):(your_password) -i -X GET http://127.0.0.1:5000/api/token

To access the protected resources use the following command:
$ curl -u (token_generated_from_above_command):unused -i -X GET http://127.0.0.1:5000/<resource> 

		-If the token is still valid the resources you can access are:
			- /listAll
			- /listAll/json  or /listAll/csv
			- /listOpenOnly
			- /listOpenOnly/json or /listOpenOnly/csv
			- /listOpenOnly/json?top=(insert int here) or /listOpenOnly/csv?top=(insert int here)
			- /listCloseOnly/json or /listCloseOnly/csv
			- /listCloseOnly/json?top=(insert int here) or /listCloseOnly/csv?top=(insert int here)
		-If your token is invalid you will be blocked from these resources

##Running the program
To run the program the cridentials file must be in the DockerMongo file locate inside the DockerRESTAPI folder.
Navigate your terminal into the DockerRestAPI folder and use the command docker-compose up --build.

## bugs
1) if the server goes down the dictionary used to store all the user objects gets emptied. if you restart the app and try to login with an existing account you won't be able to log back in due to the User object being lost. You also will not be able to re-register with the same name because that name is stored in the database and duplicate names are not allowed. This is something I will fix in the future, where I will have the user objects stored in the database itself.

2) Users using the UI do not have an extenstion to renew their token, this was a functionality that I did not have enough time to implement, but will fix in the future.



## Notes
1) If your registration on the UI is failing for a new account with a username never used before and you cant tell why it is most likely your passwords not matching for password and confirm password. This is an error message that I meant to implement but did not have the time to do so.

2) Pressing submit on a form with no control points will take you to a error page asking you to fix that mistake with a button to take you back to the page.

3) Pressing on the display button before the submit button will take you to the 'finished' page, but had a note saying if this page is empty you did not hit submit and should go back and try again with a button that takes you back to the starting page. 

4)this software will not catch if the checkpoints got inserted out of order and will return out of order times if they are. Inserting the checkpoints in the correct order is how you can ensure this program functions properly. 

5)I ran out of time and was unable to implement my flash messages that are set in some of the functions, so just ignore those.